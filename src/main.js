import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
window.axios = require('axios');
require('bootstrap');

import Vlf from 'vlf'
Vue.use(Vlf)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
